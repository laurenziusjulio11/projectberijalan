﻿using Microsoft.Extensions.Logging;
using Mopups.Hosting;

namespace FreelanceMobile;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureMopups()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			});

        

        return builder.Build();

#if DEBUG
        builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}
