using Mopups.Services;

namespace FreelanceMobile.Pages;

public partial class FormLogin 
{
	public FormLogin()
	{
        InitializeComponent();
    }

    private void LoginButton_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
    }

    private async void JoinMemberLabel_Tapped(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new RegisterPage());
        
    }

   
}