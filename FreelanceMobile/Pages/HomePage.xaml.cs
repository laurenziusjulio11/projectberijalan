using Mopups.Services;
using System.Collections.ObjectModel;

namespace FreelanceMobile.Pages;


public partial class HomePage : ContentPage
{
    

    public ObservableCollection<ImageItem> ImageItems { get; set; }
    public List<BulletPointItem> BulletPointItems { get; set; }

    private List<double> sectionPositions = new List<double>();
    private double scrollY = 0;
    public HomePage()
	{
		InitializeComponent();


        

        ImageItems = new ObservableCollection<ImageItem>
        {
            new ImageItem { ImageSource = "coding1.png", Label1 = "Website ", Label2 = ".Net, PHP, Json, HTML" },
            new ImageItem { ImageSource = "coding2.png", Label1 = "Mobile ", Label2 = "Java, React, Kotlin" },
            new ImageItem { ImageSource = "coding3.png", Label1 = "Robot Automation ", Label2 = "RPA.." },
            new ImageItem { ImageSource = "coding4.png", Label1 = "Testing ", Label2 = "Automation, Manual" }
        };

        BulletPointItems = new List<BulletPointItem>
        {
            new BulletPointItem { Text = "  .NET                                                 (1)" },
            new BulletPointItem { Text = "  PHP                                                  (9)" },
            new BulletPointItem { Text = "  Json                                                 (10)" },
            new BulletPointItem { Text = "  HTML                                                 (3)" }
         };

        BindingContext = this;
    }






        
    
	private void LoginButton_Clicked(object sender, EventArgs e)
	{
		MopupService.Instance.PushAsync(new FormLogin());
	}

    private async void RegisterButton_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new RegisterPage());
    }

    private async void SiapaKamiButton_Clicked(object sender, EventArgs e)
    {
        await scrollView.ScrollToAsync(SiapaKamiSection, ScrollToPosition.Start, true);
    }

    private async void LihatJobButton_Clicked(object sender, EventArgs e)
    {
        await scrollView.ScrollToAsync(LihatJobSection, ScrollToPosition.Start, true);
    }

    
}



    




    

public class ImageItem
{
    public string ImageSource { get; set; }
    public string Label1 { get; set; }
    public string Label2 { get; set; }
}

public class BulletPointItem
{
    public string Text { get; set; }
}


